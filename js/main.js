
$('.showmodalDown').on('click', function(){
    console.log(131123123)
    $('.modalShow').show();
    $('body').addClass('modalShowBody')
    $('.custom-modal-wrp').show();
    $('.custom-modal').slideDown(300);
})

// HEADER MOB NAV

$('.trigger-nav').on('click', function () {
    $('.nav-header ul').slideToggle( "slow" );
})

$('.modalShow').on('click', function(){
    $('.modalShow').hide();
    $('body').removeClass('modalShowBody')
    $('.custom-modal').slideUp(300);
})

// HEADER MOB NAV END

// SCROLL
$(document).ready(function(){
    $(".nav-header").on("click","a", function (event) {
        event.preventDefault();
        var id  = $(this).attr('href'),
        top = $(id).offset().top;
        $('body,html').animate({scrollTop: top - 80}, 1200);
        var _this = this;
        // $(".nav-header li a").removeClass('active')
        // $(this).addClass('active')
        if (window.innerWidth < 991) {
            $('.trigger-nav').click();
        }
    });
});

$('.tab-next').on('click', function(){
    if($('.home-tabs').hasClass('active')){
        $('.home-tabs').removeClass('show active')
        $('.advisory-tabs').addClass('show active');
        $('.home-tab-content').removeClass('show active')
        $('.advisory-tab-content').addClass('show active');
    } else{
        if($('.advisory-tabs').hasClass('active')){
            $('.advisory-tabs').removeClass('show active')
            $('.developers-tabs').addClass('show active');
            $('.advisory-tab-content').removeClass('show active')
            $('.developers-tab-content').addClass('show active');
        }else{
            if($('.developers-tabs').hasClass('active')){
                $('.developers-tabs').removeClass('show active');
                $('.developers-tab-content').removeClass('show active');
                $('.home-tabs').addClass('show active')
                $('.home-tab-content').addClass('show active')
            }
        }
    }
})

$('.tab-prev').on('click', function(){

    if($('.developers-tabs').hasClass('active')){
        $('.advisory-tabs').addClass('show active')
        $('.developers-tabs').removeClass('show active');
        $('.advisory-tab-content').addClass('show active')
        $('.developers-tab-content').removeClass('show active');
    }else{
        if($('.advisory-tabs').hasClass('active')){
            $('.home-tabs').addClass('show active')
            $('.advisory-tabs').removeClass('show active');
            $('.home-tab-content').addClass('show active')
            $('.advisory-tab-content').removeClass('show active');
        }else{
             if($('.home-tabs').hasClass('active')){
                $('.home-tabs').removeClass('show active')
                $('.home-tab-content').removeClass('show active')
                $('.developers-tabs').addClass('show active');
                $('.developers-tab-content').addClass('show active');
            }
        }
    }
})


$('.scrollToTop').click(function(){
    $("html, body").animate({ scrollTop: 0 }, 600);
    return false;
});


var menu_selector = ".nav-header"; 
function onScroll(){
	var scroll_top = $(document).scrollTop();
	$(menu_selector + " a").each(function(){
		var hash = $(this).attr("href");
		var target = $(hash);
		if (target.position().top - 110 <= scroll_top && target.position().top + target.outerHeight() > scroll_top) {
			$(menu_selector + " a.active").removeClass("active");
			$(this).addClass("active");
		} else {
			$(this).removeClass("active");
		}
	});
}
$(document).ready(function () {
	$(document).on("scroll", onScroll);
	$("a[href^=#]").click(function(e){
		e.preventDefault();
		$(document).off("scroll");
		$(menu_selector + " a.active").removeClass("active");
		$(this).addClass("active");
		var hash = $(this).attr("href");
		var target = $(hash);
		$("html, body").animate({
		    scrollTop: target.offset().top
		}, 500, function(){
			window.location.hash = hash;
			$(document).on("scroll", onScroll);
		});
	});
});

$(document).ready(function(){
	$(".nav-header").changeActiveNav();
});


