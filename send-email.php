<?php 
session_start();

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

require './PHPMailer/src/Exception.php';
require './PHPMailer/src/PHPMailer.php';
require './PHPMailer/src/SMTP.php';

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    if (array_key_exists('mail_sent', $_SESSION)) {
        if (time() - $_SESSION['mail_sent'] > 300) {
            unset($_SESSION['mail_sent']);
            sendEmail();
        }
    } else {
        sendEmail();
    }
}

function sendEmail() {
    $mailTo = 'contact@peakdata.ch';

    //credentials 
    $username = 'testdev322@gmail.com';
    $password = 'qwerty111';

    $mail = new PHPMailer(true);

    try {

        //Server settings
        $mail->SMTPDebug = 0;                                 // Enable verbose debug output
        $mail->isSMTP();                                      // Set mailer to use SMTP
        $mail->Host = 'smtp.gmail.com';                       // Specify main and backup SMTP servers
        $mail->SMTPAuth = true;                               // Enable SMTP authentication
        $mail->Username = $username;                          // SMTP username
        $mail->Password = $password;                          // SMTP password
        $mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
        $mail->Port = 587;                                    // TCP port to connect to

        //Recipients
        $mail->setFrom($mailTo, 'Labscape');
        $mail->addAddress($mailTo);                           // Add a recipient

        //Content
        $mail->isHTML(true);                                  // Set email format to HTML
        $mail->Subject = 'New demo request';
        $mail->Body    = 'name: ' . $_POST['name'] . '<br>' .
            'email: ' . $_POST['email'];
        $mail->send();
        
        $_SESSION['mail_sent'] = time();
    } catch (Exception $e) {
        echo 'Message could not be sent. Mailer Error: ', $mail->ErrorInfo;
    }
}

header('Location: ./index.htm');

?>